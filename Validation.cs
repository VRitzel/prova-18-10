﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace Prova_18_10
{
    public class Validation
    {
        public bool IsNameNull(string name, string message)
        {
            string value = name;
            value.Trim();
            if (value.Length == 0) {
                throw new ArgumentException(message);
            }
            return true;
        }

        public bool IsPriceValid(decimal price, string message)
        {
            if (price <= 0)
            {
                throw new ArgumentException(message);
            }
            return true;
        }
    }
}
