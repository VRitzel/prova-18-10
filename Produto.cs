﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova_18_10
{
    public class Produto
    {
        public Guid Id { get; private set; }
        public string Name { get; private set;}
        public decimal Price { get; private set;}

        public Produto(Guid id, string name, decimal price)
        {
            Id = id;
            Name = name;
            Price = price;
        }
        public void Validacao()
        {
            Validation.IsNameNull(Name, "O nome não pode ser vazio");

            Validation.IsPriceValid(Price, "O preço não pode ser menor ou igual a 0");
        }
    }
}
