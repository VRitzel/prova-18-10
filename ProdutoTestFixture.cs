﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova_18_10
{
    public class ProdutoTestFixture : IClassFixture<ProdutoTestFixture>
    {
        public Produto ProductIsValid()
        {
            return new(
                Guid.NewGuid(),
                "Relógio",
                100
                );
        }

        public Produto NameIsInvalid()
        {
            return new(
                Guid.NewGuid(),
                "",
                100
            );
        }

        public Produto PriceIsInvalid()
        {
            return new(
                Guid.NewGuid(),
                "Espelho",
                0
                );
        }
    }
}
