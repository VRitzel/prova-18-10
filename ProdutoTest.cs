namespace Prova_18_10
{
    public class ProdutoTest : IClassFixture<ProdutoTestFixture>
    {
        private ProdutoTestFixture _fixture;

        public ProdutoTest(ProdutoTestFixture fixture) {
            _fixture = fixture;
        }
        [Fact(DisplayName = "Valid Product")]
        public void ProductIsValid_ShouldReturnTrueWithNoErrors()
        {
            // arrange
            var product = _fixture.ProductIsValid;

            // assert
            Assert.True(product);
        }
        [Fact(DisplayName = "Invalid Name")]
        public void NameIsNotValid_IsNull_ShouldReturnFalseWithErrors()
        {
            // arrange
            var name = _fixture.NameIsInvalid;

            Assert.Null(name);
        }
        [Fact(DisplayName = "Invalid Price")]
        public void PriceIsNotValid_IsNullOrLesserThan0_ShouldReturnFalseWithErrors()
        {
            var value = _fixture.PriceIsInvalid;
            

            Assert.True(value)
        }
    }
}